#!/usr/bin/env raku

use Cro::HTTP::Client;
use Cro::HTTP::BodyParsers;
use JSON::Fast;
use Term::Choose;

# CONFIG
my @subreddits = ("wallpapers", "wallpaper");
my $sort = "hot";
my $limit = 10;
my $filename-remove-regex = / \s+ || \[.*\] || \(.*\) || \. /;

# CONFIG default output directory $XDG_PICTURES_DIR/wpr/
my $xdg-pictures = (run "xdg-user-dir", "PICTURES", :out).out.get;
my $folder = $xdg-pictures ~ "/wpr/";
mkdir $folder;

# CONFIG default output directory for saving images $XDG_PICTURES_DIR/wpr/
my $save-folder = $xdg-pictures ~ "/GoodWallpapers/";
mkdir $save-folder;

# PROGRAM

# Get all uploads.
my @all;
for @subreddits -> $subreddit {
    my @data = get($subreddit);
    @all.append: @data;
}

# Sort and limit.
@all = @all.sort: *<score>;
@all = @all.reverse;
@all = @all.head($limit);

# Display
my @pretty = @all.map(&prettify);
@pretty.append: "save";
my $default = 0;
loop {
    my $index = choose(@pretty, :2layout, :1index, :1save-screen, :default($default));

    with $index {
        $default = $index;
        if $index < @all.elems {
            set-wp(@all[$index]);
        } else {
            save();
        }
    } else {
        last;
    }
}

sub save() {
    for IO::Path.new($folder).dir() -> $file {
        my $to = IO::Path.new($save-folder ~ $file.basename);
        my $data = slurp $file, :bin;
        spurt $to, $data, :createonly;
    }
}

sub set-wp($item) {
    # Download
    my $resp = await Cro::HTTP::Client.get($item<url>);
    my $picture = await $resp.body;

    # Remove all existing files.
    for IO::Path.new($folder).dir() -> $file {
        unlink $file.path;
    }

    my $filename = $item<title>.wordcase.subst($filename-remove-regex, :g);

    # Write to file
    my $out-wallpaper = $folder ~ $filename ~ "." ~ $item<url>.split(".").reverse[0];

    my $file = open $out-wallpaper, :w, :create;
    $file.write: $picture;
    $file.flush;
    $file.close;

    # Run command
    run "feh", "--bg-fill", $out-wallpaper;
}

sub prettify($item) {
    return $item<title> ~ "(" ~ $item<subreddit> ~ ")";
}

sub get($subreddit) {
    my $resp = await Cro::HTTP::Client.get("https://reddit.com/r/$subreddit/$sort.json");
    my %json = await $resp.body;

    my @data := %json<data><children>;

    my @filtered = @data.grep(&filter-matching).map(&map-simple);
    return @filtered;
}

sub filter-matching($data) {
    return !$data<data><is_video>;
}

sub map-simple($data) {
    my %hash = title => $data<data><title>, url => $data<data><url>, subreddit => $data<data><subreddit>, score => $data<data><score>;
    return %hash;
}
