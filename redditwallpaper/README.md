# redditwallpaper

Select, download and set a wallpaper from reddit.

## Dependencies

### Raku-Modules

- `Cro::HTTP`
- `JSON::Fast`
- `Term::Choose`

### External

- `feh`

## Run

Just run the executable, wait for the metadata of the wallpapers beeing downloaded, select one wallpaper using `j` (Down) and `k` (Up), click Enter to set the wallpaper, `q` to quit the selection.

## Configuration

You can configure the subreddits to query the wallpapers for (by default "r/wallpaper" and "r/wallpapers"), the default download directory (by default `$XDG_PICTURE_DIR/wpr/`) and the sort order (by default `hot`) by changing the first few lines directly in the script.
