# Raku Scripts

This repository contains some scripts written in [raku](https://www.raku.org/), so make sure the programming language is installed. Most scripts also need additional dependencies, they will be listed in their READMEs.

## Installing modules

For every script, the required modules will be listed. Intall them using:

```
zef install <module>
```

## Scripts

### greptui

A TUI-frontend for grep.

### redditwallpaper

A program to choose, download and set the wallpaper from reddit.

## License

All scripts are published under the GPL-3.0-or-later.
