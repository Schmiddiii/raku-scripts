# greptui

A TUI-frontend for grep.

## Dependencies

### Raku-Modules

- `Term::Choose`

### External

- `grep`
- `nvim` (configurable to also be any other editor)

## Usage

```
gt <search> [<path>]
```

`<search>` is any search pattern to search for, `<path>` is the path of a file or folder to search in, defaulting to `.`.

This will open a TUI where all matches are listed, select a element using `j` (Down) or `k` (Up) and click enter to jump to the file and line in `nvim`. Quit the selection using `q`.
