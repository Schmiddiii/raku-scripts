#!/usr/bin/env raku

use Terminal::ANSI::OO 't';
use Term::Choose :choose;

sub greptui($path, $search) {
    # Execute grep.
    my $command = run "/bin/grep", "-R", "-n", $path, $search, :out; 
    my $output = $command.out.slurp;
    my @lines = $output.split("\n", :skip-empty)>>.split(":", 3);

    my @pretty-lines = map {if (split("/", .[0], 3).elems == 3) {
        (".../" ~ split("/", .[0]).tail, .[2])
    } else {
        (.[0], .[2])
    }}, @lines;

    # Align everything
    my $longest-path = 0;
    for @pretty-lines -> @line {
        if @line[0].chars > $longest-path {
            $longest-path = @line[0].chars;
        }
    }

    my @pretty-lines-format = map {(.[0] ~ " " x ($longest-path - .[0].chars + 1) ~ "|" , .[1])}, @pretty-lines;

    my $default = 0;
    loop {
        # Choose a line forever.
        my $chosen = choose(@pretty-lines-format, :2layout, :1save-screen, :default($default), :1index);
        # If something was chosen.
        with $chosen {
            # Move the default selection to this element.
            $default = $chosen;
            # And open nvim.
            run "nvim", "+@lines[$chosen][1]", "@lines[$chosen][0]";
        } else {
            # Otherwise exit.
            last;
        }
    }
}

sub MAIN(Str $search, IO() $path where {$path.IO.d or $path.IO.f} = ".") {
    greptui($search, $path);
}
